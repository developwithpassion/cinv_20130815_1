namespace :git do
  desc 'set up the remotes for git'
  task :candidates do
    configatron.git.remotes.each do|remote|
      `git remote rm #{remote}`
      `git remote add #{remote} https://bitbucket.org/#{remote}/#{configatron.git.repo}`
    end
  end

end
